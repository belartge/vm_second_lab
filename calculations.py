from triple_running import running


class Calculations:
    vector_X = []
    debug = False
    # vector_Y = [0, 3, 28, 117, 336]
    vector_Y = []

    # краевое условие - на обоих концах вторая производная
    A = 0
    B = 0

    def __init__(self, x, y, A, B):
        self.vector_Y = y
        self.vector_X = x
        self.A = A
        self.B = B

    def reinit(self,x,y,A,B):
        self.vector_Y = y
        self.vector_X = x
        self.A = A
        self.B = B

    A_coeffs = []
    B_coeffs = []
    C_coeffs = []
    D_coeffs = []

    def get_h_i(self, i):
        if i > len(self.vector_X):
            return 0
        else:
            return self.vector_X[i] - self.vector_X[i - 1]

    def get_s_i(self, i, x):
        return self.A_coeffs[i] + self.B_coeffs[i] * (x - self.vector_X[i]) + self.C_coeffs[i] / 2.0 * (
                x - self.vector_X[i]) ** 2 + self.D_coeffs[i] / 6.0 * (x - self.vector_X[i]) ** 3

    def get_F_i(self, i):
        first = (self.vector_Y[i + 1] - self.vector_Y[i]) / self.get_h_i(i + 1)
        second = (self.vector_Y[i] - self.vector_Y[i - 1]) / self.get_h_i(i)
        return first - second

    def get_necessary_zeros(self, start, end):
        ans = []
        for i in range(start, end):
            ans += [0]
        return ans

    def prepare_matrix(self):
        first_string = [1]
        last_string = [1]
        size = len(self.vector_X)
        for i in range(1, size):
            first_string = first_string + [0]
            last_string = [0] + last_string
        the_matrix = [first_string]
        for i in range(1, size - 1):
            the_string = self.get_necessary_zeros(0, i - 1) + [self.get_h_i(i),
                                                               2 * (self.get_h_i(i) + self.get_h_i(i + 1)),
                                                               self.get_h_i(i + 1)] + self.get_necessary_zeros(i + 1,
                                                                                                               size - 1)
            the_matrix += [the_string]
        the_matrix += [last_string]
        return the_matrix

    def prepare_Fs(self):
        the_f_vector = [self.A]
        for i in range(1, len(self.vector_X) - 1):
            new_f = self.get_F_i(i) * 6
            the_f_vector += [new_f]
        the_f_vector += [self.B]
        return the_f_vector

    def calculate(self):
        self.get_c_coeffs()
        self.calculate_d()
        self.calculate_b()
        self.calculate_a()

    def get_c_coeffs(self):
        self.C_coeffs = []
        matrix = self.prepare_matrix()
        fs = self.prepare_Fs()
        run = running(matrix, fs)
        self.C_coeffs = run.calculate()
        if self.debug:
            print("C coeffs,", self.C_coeffs)

    def calculate_d(self):
        self.D_coeffs = [0.0]
        for i in range(1, len(self.C_coeffs)):
            self.D_coeffs += [(self.C_coeffs[i] - self.C_coeffs[i - 1]) / self.get_h_i(i)]
        if self.debug:
            print("D coeffs,", self.D_coeffs)

    def calculate_b(self):
        self.B_coeffs = [0.0]
        for i in range(1, len(self.C_coeffs)):
            h = self.get_h_i(i)
            c = self.C_coeffs[i]
            d = self.D_coeffs[i]
            part = h * c / 2 - h ** 2 * d / 6 + (self.vector_Y[i] - self.vector_Y[i - 1]) / h
            self.B_coeffs += [part]
        if self.debug:
            print("B coeffs,", self.B_coeffs)

    def calculate_a(self):
        self.A_coeffs = []
        for i in range(0, len(self.vector_Y)):
            self.A_coeffs += [self.vector_Y[i]]
        if self.debug:
            print("A coeffs,", self.A_coeffs)

    def get_x_index(self, x):
        for i in range(0, len(self.vector_X) - 1):
            left = self.vector_X[i]
            right = self.vector_X[i + 1]
            if x == left:
                return i
            if left < x <= right:
                return i + 1
        return -1

    def get_value_in(self, point):
        if len(self.C_coeffs) == 0 and len(self.vector_X) > 1:
            self.calculate()
        index = self.get_x_index(point)
        if index > -1:
            a = self.A_coeffs[index]
            b = self.B_coeffs[index]
            c = self.C_coeffs[index]
            d = self.D_coeffs[index]
            return a + b * (point - self.vector_X[index]) + c / 2 * (
                    point - self.vector_X[index]) ** 2 + d / 6 * (
                           point - self.vector_X[index]) ** 3
