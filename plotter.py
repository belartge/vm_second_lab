import os

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from matplotlib import gridspec
from matplotlib.gridspec import GridSpec
from matplotlib.widgets import Button
from py_expression_eval import Parser
import easygui

from file_worker import FileReader
from calculations import Calculations

# matplotlib.interactive(True)
plt.switch_backend("TkAgg")

# plt.figure(1)
red_line = mlines.Line2D([], [], color='red', markersize=15, label='Построенный сплайн')
blue_line = mlines.Line2D([], [], color='blue', markersize=15, label='Исходная функция')


class Index(object):
    ind = 0

    def open(self):
        return easygui.fileopenbox()


class Plotty:
    path_to_file = "tests/test10.txt"

    fr = FileReader()
    expression = ""
    calc = None
    step = 0.1

    x = np.array([])
    y = np.array([])
    real_y = []

    def f(self, x):
        return self.calc.get_value_in(x)

    def real_f(self, x):
        parser = Parser()
        return parser.parse(self.expression).simplify({'x': x}).toString()

    def initialize_with(self, file):
        self.path_to_file = file
        self.x = []
        self.y = []
        self.calc = None
        # self.open_file()

    def open_file(self):
        self.path_to_file = Index().open()
        print("file picked ", self.path_to_file)
        temp = self.path_to_file
        temp = temp.replace("P:\\Codes\\Python\\cubic_sline\\", "")
        temp = temp.replace("\\", "/")
        self.path_to_file = temp
        print("file path configured ", self.path_to_file)
        self.prepare()

    def checkValidityLen(self, vector):
        return len(vector) > 2

    def checkValiditySort(self, vector):
        for i in range(0, len(vector) - 1):
            if vector[i] > vector[i+1]:
                return False
        return True

    def prepare(self):
        parsed_data = self.fr.reading_file(self.path_to_file)
        if parsed_data is None:
            print("IER=0: dumb")
            return
        if not self.checkValidityLen(parsed_data['vector_x']):
            print("IER=1: недостаточно точек")
        if not self.checkValiditySort(parsed_data["vector_x"]):
            print("IER=2: нарушен порядок следования аргументов")
            return
        self.calc = None
        self.calc = Calculations(x=parsed_data["vector_x"], y=parsed_data["vector_y"], A=parsed_data["A"],
                                 B=parsed_data["B"])

        dot_from = self.calc.vector_X[0]
        dot_to = self.calc.vector_X[len(self.calc.vector_X) - 1]
        self.x = np.arange(dot_from, dot_to, self.step)
        self.expression = parsed_data["expression"]
        self.calc.calculate()
        self.calculate_ys()
        self.draw_graphs()

    def calculate_ys(self):
        y = []
        real_y = []
        x = []
        for i in self.x:
            x.append(i)
            y.append(self.f(i))
            real_y.append(self.real_f(i))

        y += [y[len(y) - 1]]
        real_y += [real_y[len(real_y) - 1]]
        x += [x[len(x) - 1]]
        self.x = np.array(x)
        self.y = np.array(y)
        self.real_y = np.array(real_y)

    def draw_graphs(self):
        plt.clf()

        fig = plt.figure(1)

        gs = GridSpec(3, 2)  # 2 rows, 2 columns

        ax1 = fig.add_subplot(gs[0, 0])  # First row, first column
        ax1.plot(self.x, self.y, 'red')
        ax2 = fig.add_subplot(gs[0, 1])  # First row, second column
        ax2.plot(self.x, self.real_y, 'blue')
        ax4 = fig.add_subplot(gs[1:, :])  # Second row, span all columns
        ax4.plot(self.x, self.y, 'red')
        ax4.plot(self.x, self.real_y, 'blue')

        blue_line = mlines.Line2D([], [], color='blue', markersize=15, label='Исходная функция y(x) = ' + self.expression)

        # Shrink current axis's height by 10% on the bottom
        box = ax4.get_position()
        ax4.set_position([box.x0, box.y0 + box.height * 0.1,
                         box.width, box.height * 0.9])

        # Put a legend below current axis
        ax4.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), handles=[red_line, blue_line],
                  fancybox=True, shadow=True, ncol=5)

        plt.show()

