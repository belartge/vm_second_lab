from math import sin


class FileReader:
    def string_to_list(self, vector_line):
        vector_line = vector_line.replace('\n', "")
        vector_line = vector_line.split()

        vector = []

        for x in vector_line:
            vector.append(float(x))

        return vector

    def reading_file(self, file_name):
        the_file = open(file_name)

        vector_x_line = ""
        vector_y_line = ""
        expression = ""
        a_line = ""
        b_line = ""

        for line in the_file:
            if "[X]: " in line:
                vector_x_line = line.replace("[X]: ", "")
            elif "[Y]: " in line:
                vector_y_line = line.replace("[Y]: ", "")
            elif "[A]: " in line:
                a_line = line.replace("[A]: ", "")
            elif "[B]: " in line:
                b_line = line.replace("[B]: ", "")
            elif "[f(x)]: " in line:
                expression = line.replace("[f(x)]: ", "")
                expression = expression.replace("\n", "")
            else:
                print("Something went wrong")
                return None

        return {
            "vector_x": self.string_to_list(vector_x_line),
            "vector_y": self.string_to_list(vector_y_line),
            "expression": expression,
            "A": float(a_line),
            "B": float(b_line)
        }
