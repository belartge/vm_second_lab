import os

from plotter import *


def greetings():
    print("Построение графика функции при помощи интерполяции кубическими сплайнами")
    print("Выполнил Яппаров Артем")


def get_available_num():
    list = os.listdir(os.getcwd())  # dir is your directory path
    count = 0
    for x in list:
        if "test" in x:
            count += 1
    return count


def get_available():
    list = os.listdir(os.getcwd())  # dir is your directory path
    answer = []
    for x in list:
        if "test" in x and "result" not in x:
            answer.append(x)
    print(answer)
    return answer


def is_file_available(name):
    list = os.listdir(os.getcwd())
    print(name)
    if name in list:
        return True
    return False


def pick_the_file():
    print("В данный момент в системе доступно", get_available_num(), "тестовых файлов")
    file = input("Введите номер теста (нумерация с 0): ")
    if is_file_available("test" + file + ".txt"):
        return file
    print("Увы, такой файл недоступен")
    return ""


def main():
    greetings()
    while True:
        calculator = Plotty()
        print("Выберите способ ввода данных:")
        print("\t1. Выбрать среди тестовых файлов")
        print("\t2. Просчитать все тестовые файлы")
        choice = input("> ")
        if choice == "1":
            ans = pick_the_file()
            if ans != "":
                calculator.initialize_with(file="test" + ans + ".txt")
                calculator.prepare()
        elif choice == "2":
            ans = get_available()
            for file in ans:
                print("Тестовый файл", file, end=":   ")
                calculator.initialize_with(file=file)
                calculator.prepare()
        elif choice == "0":
            return
        else:
            print("Введена некорректная команда")
        print("")
        print("=========================================================================")
        print("")


main()
