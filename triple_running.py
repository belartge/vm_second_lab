class running:
    the_matrix = [[]]
    coeffs_A = []
    coeffs_B = []
    coeffs_C = []
    f = []

    mu = []
    nu = []

    def __init__(self, matrix, f):
        self.the_matrix = matrix
        self.f = f

    def prepare(self):
        size = len(self.the_matrix)
        self.coeffs_A = []
        self.coeffs_B = []
        self.coeffs_C = []
        self.mu = []
        self.nu = []
        for x in range(0, size):
            self.coeffs_C += [self.the_matrix[x][x]]
            if x > 0:
                self.coeffs_A += [self.the_matrix[x][x - 1]]
            else:
                self.coeffs_A += [0]
            if x < size - 1:
                self.coeffs_B += [self.the_matrix[x][x + 1]]
            else:
                self.coeffs_B += [0]

    def calculate(self):
        self.prepare()
        self.calculate_forward()
        return self.calculate_backward()

    def calculate_forward(self):
        self.mu += [0, - float(self.coeffs_B[0]) / self.coeffs_C[0]]
        self.nu += [0, self.f[0] / self.coeffs_C[0]]

        for i in range(1, len(self.coeffs_C)-1):
            D = self.coeffs_C[i] + self.coeffs_A[i] * self.mu[i]
            self.mu += [- float(self.coeffs_B[i]) / D]
            self.nu += [(self.f[i] - self.coeffs_A[i] * self.nu[i]) / D]

    def calculate_backward(self):
        end = len(self.f) - 1
        answer_list = [(self.f[end] - self.coeffs_A[end] * self.nu[end]) / (
                    self.coeffs_A[end] * self.mu[end] + self.coeffs_C[end])]
        for i in range(end, 0, -1):
            x = answer_list[0]
            mu = self.mu[i]
            nu = self.nu[i]
            answer_list = [x * mu + nu] + answer_list
        return answer_list
